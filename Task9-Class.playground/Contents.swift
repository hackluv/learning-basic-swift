import UIKit

class BlogPost {
    
    var title = ""
    var body = ""
    var author = ""
    var numberOfComments = 0
    
    func addComment() {
        numberOfComments += 1
    }
    
}

let myPost = BlogPost()
myPost.title = "Hello Playground"
myPost.body = "lolem ipsum"
myPost.author = "admin"
myPost.addComment()

print(myPost.numberOfComments)

let mySecondPost = BlogPost()
myPost.title = "Goodbey Playground"
myPost.body = "lolem ipsum"
myPost.author = "Long"

print(mySecondPost.numberOfComments)
