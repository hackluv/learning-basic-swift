import UIKit

var str = "Hello, playground"

var counter = -5

// while
while counter > 0 {
    print("hello")
    counter = counter - 1
}

// alternative do while
repeat{
    print("Hello 2")
    counter = counter - 1
}while  counter > 0
