import UIKit

var str = "Hello, playground" // declare variable
print(str)

var num = 1
num = 2
print(num)

str = String(20) // casting type
print(str)

var a = 10
var b = 20

print(a + b)
print(a - b)
print(a * b)
print(a / b)

let c = 20 // constant
