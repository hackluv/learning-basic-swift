import UIKit

var str = "Hello, playground"

// Function return values

func addTwoNumbers() -> Int {
    
    let a = 1
    let b = 1
    let c = a + b
    
    return c
}

let sum = addTwoNumbers()

print(sum)

// Function with 1 Parameter
func substractTwoNumber(arg para:Int) -> Int {
    let a = para
    let b = 2
    
    return a - b
}

let sub = substractTwoNumber(arg: 5)

print(sub)

// Function with multiple Parameter
func multipleTwoNumber(number1:Int, number2:Int) -> Int {
    let a = number1
    let b = number2
    
    return a * b
}

let mul = multipleTwoNumber(number1: 10, number2: 5)

print(mul)

